import unittest
import password_check


class PasswordsTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_password_digits(self):
        """
        Test public users
        """
        self.assertFalse(password_check._check_numbers("test"))
        self.assertTrue(password_check._check_numbers("test23"))

    def test_password_caps(self):
        """
        Test public users
        """
        self.assertFalse(password_check._check_caps("test"))
        self.assertTrue(password_check._check_caps("Test23"))

    def test_length(self):
        """
        Test public users
        """
        self.assertFalse(password_check._check_caps("test"))
        self.assertTrue(password_check._check_caps("Test23afsefesfsaefsefsfe"))

if __name__ == '__main__':
    unittest.main()