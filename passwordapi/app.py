from flask import Flask, request
# from passwordapi.passwords import password_check
import  passwords
app = Flask(__name__)


@app.route('/password_check', methods=['GET'])
def hello_world():
    req_data = request.get_json()
    print(req_data['password'])
    return str(passwords.password_check.password_check(req_data['password']))


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')