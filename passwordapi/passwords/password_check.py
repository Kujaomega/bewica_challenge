import re
import sys

password_params = {
  "length": 10,
  "must_have_numbers": True,
  "must_have_caps": True
}


def password_check(password):
    correct = True
    if password_params['must_have_numbers']:
        correct = _check_numbers(password)
    if password_params['must_have_caps']:
        correct = _check_caps(password)
    if password_params['length']:
        correct = _check_length(password)
    print(correct)
    return correct


def _check_numbers(password):
    return bool(re.search(r'\d', password))


def _check_caps(password):
    return password != password.lower()


def _check_length(password):
    return len(password) >= password_params['length']


if __name__ == "__main__":
    password_check(sys.argv[1])
